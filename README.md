# Powercord Scripts

Collection of console scripts for [Powercord](https://powercord.dev/)

## Call

```javascript
(async ()=>{
  const {
    getModule,
    messages: { receiveMessage },
    channels: { getChannelId },
  } = require('powercord/webpack')
  const { createBotMessage } = await getModule(['createBotMessage'])
  const { getCurrentUser } = await getModule(['getCurrentUser'])
  
  let msg = createBotMessage(getChannelId(), 'Helo')
  msg.author = getCurrentUser()
  msg.mention_everyone = false
  msg.type = 3
  msg.flags = 0
  receiveMessage(msg.channel_id, msg)
})()
```


## Boost

```javascript
(async ()=>{
  const {
    getModule,
    messages: { receiveMessage },
    channels: { getChannelId },
  } = require('powercord/webpack')
  const { createBotMessage } = await getModule(['createBotMessage'])
  const { getCurrentUser } = await getModule(['getCurrentUser'])
  
  let msg = createBotMessage(getChannelId(), 'lol im a booster')
  msg.author = getCurrentUser()
  msg.mention_everyone = false
  msg.type = 8
  msg.flags = 0 
  receiveMessage(msg.channel_id, msg)
})()
```

## Boost level 1

```javascript
(async ()=>{
  const {
    getModule,
    messages: { receiveMessage },
    channels: { getChannelId },
  } = require('powercord/webpack')
  const { createBotMessage } = await getModule(['createBotMessage'])
  const { getCurrentUser } = await getModule(['getCurrentUser'])
  
  let msg = createBotMessage(getChannelId(), 'SpoofMyBoost')
  msg.author = getCurrentUser()
  msg.mention_everyone = false
  msg.type = 9 
  msg.flags = 0
  receiveMessage(msg.channel_id, msg)
})()
```

## Boost level 2
```javascript
(async ()=>{
  const {
    getModule,
    messages: { receiveMessage },
    channels: { getChannelId },
  } = require('powercord/webpack')
  const { createBotMessage } = await getModule(['createBotMessage'])
  const { getCurrentUser } = await getModule(['getCurrentUser'])
  
  let msg = createBotMessage(getChannelId(), 'SpoofMyBoost')
  msg.author = getCurrentUser()
  msg.mention_everyone = false
  msg.type = 10
  msg.flags = 0
  receiveMessage(msg.channel_id, msg)
})()
```

## Boost level 3
```javascript
(async ()=>{
  const {
    getModule,
    messages: { receiveMessage },
    channels: { getChannelId },
  } = require('powercord/webpack')
  const { createBotMessage } = await getModule(['createBotMessage'])
  const { getCurrentUser } = await getModule(['getCurrentUser'])
  
  let msg = createBotMessage(getChannelId(), 'SpoofMyBoost')
  msg.author = getCurrentUser()
  msg.mention_everyone = false
  msg.type = 11
  msg.flags = 0
  receiveMessage(msg.channel_id, msg)
})()
```

## Previews

<img src="https://belle.is-inside.me/T2n3Z7Zz.png"/>
<img src="https://belle.is-inside.me/ZgClHK4E.png"/>
<img src="https://belle.is-inside.me/ZgfUdEiw.png"/>
<img src="https://belle.is-inside.me/0YsmxZUr.png"/>
<img src="https://belle.is-inside.me/Ubf7cz0y.png"/>

## Credit
Credit [Ignacio](https://gitdab.com/settings)

## Discord
Syz#0001 Send Nudes